#include <stdio.h>
#include <string.h>

struct _Person
{
    char name[30];
    int heightcm;
    double weightkg;
};

typedef struct _Person Person;

int main(int argc, char* argv[])
{
    Person Peter;
    strcpy(Peter.name, "Peter");
    Peter.heightcm = 175;
    Peter.weightkg = 78.7;   
    printf("Peter's Height: %i Peter´s weight: %2f/n", Peter.heightcm, Peter.weightkg);
   

    return 0;
}
