#include <stdio.h>
#include <string.h>

struct _Person
{
    char name[30];
    int heightcm;
    double weightkg;
};
// This abbreviates the type name
typedef struct _Person Person;
typedef Person* PPerson;

int main(int argc, char* argv[])
{
    Person Peter;
    PPerson pPeter;
    
    pPeter = &Peter;
  
    strcpy(Peter.name, "Peter");
    Peter.heightcm = 175;
    Peter.weightkg = 80.86;
    strcpy(pPeter-> name, "Pepuso");
    printf ("Pepuso es igual que Peter Pepuso peso %2f Pepuso altura %i\n  ", pPeter-> weightkg, pPeter-> heightcm);
    printf ("Peter peso %2f Pepuso altura %i\n  ", Peter.weightkg, Peter.heightcm);
    pPeter -> weightkg = 81.3;
   
    pPeter -> heightcm = 176;
   
   printf ("Pepuso ha cambiado por lo que Peter cambia ya que Pepuso es un pointer de Person que señala a Peter\n ");
   printf ("Pepuso es distinto a Peter; Pepuso peso %2f Pepuso altura %i\n  ", pPeter-> weightkg, pPeter-> heightcm);
  
   printf ("Peter peso %2f Pepuso altura %i\n  ", Peter.weightkg, Peter.heightcm);

    // Show the information of the Peter data structure on the screen

    return 0;
}
