#include <stdio.h>


 double *pd;
 double d;

void f(double * pd)
{

  d = 4; //d variable is set to 4
  pd = &d; //The Address of variable *pd is now the Address of d
  printf("Value of *pd in f: %f\n", *pd);
   
  d = *pd; //We assign the value *pd is pointing,  not necessary
  printf("Value of d in f: %f\n", d); 
}

int main(int argc, char* argv[])
{


    d = 3;

    pd = &d;
    f(pd);
    
    printf("%f\n", d);

   /* access the value using the pointer */
    printf("Value of *pd variable: %f\n",*pd );
    return 0;
}
