;
; Compares if the length of two strings is the same
;
    .data

string1:       .asciiz "whatever1"   ; first string
string2:       .asciiz "whatever2"   ; second string
result:        .word 0 ;
    .code
    main:
     xor r16,r16,r16 ;
     xor r17,r17,r17 ; 
     xor r8,r8,r8 ;
    
    loop_1:
	
     lbu r4,string1(r16);
     beqz r4,loop_2; first string ends
     daddui r16,r16,1; next index
     j loop_1;
	 
    loop_2:
     lbu r4,string2(r17);
     beqz r4,final; second string ends
     daddui r17,r17,1; next index
     j loop_2;
	 
    final:
       beq r16 ,r17, equals;
       j end ;
    equals:
       daddui r8,r8,1
    end:
      sd r8, result(r0);
      
      halt
