;
; Search for the length of a character String
;
    .data
string: .asciiz "12345" ;
result: .word  0 ; 


    .code
main:
    xor    r16, r16, r16   ; 
    
loop:

     lbu r4,string(r16);
     beqz r4,end; first string ends
     daddui r16,r16,1; next index
     j loop;

end:
    sd     r16, result(r0) ; save result
    halt

